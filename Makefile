iEdit: main.o editor.o text_line.o
	g++ main.o editor.o text_line.o -o iEdit
main.o: main.h editor.h text_line.h main.cpp
	g++ -c main.cpp
editor.o: editor.h text_line.h editor.cpp
	g++ -c editor.cpp
text_line.o: text_line.h text_line.cpp
	g++ -c text_line.cpp
clean:
	rm -rf *.o iEdit
